package com.bh.conf;

/*
Andrii Maliuta, Jan 28, 2019
 */

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Scanned
public class ActualPages implements Macro {

    private final String TEMPLATE = "templates/template.vm";

    private final SpaceManager spaceManager;

    @Autowired
    public ActualPages(@ComponentImport SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public String execute(Map<String, String> params,
                          String s,
                          ConversionContext conversionContext) throws MacroExecutionException {

        Map<String, Object> context = MacroUtils.defaultVelocityContext();

        String dateParam = params.get("date");

//        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        LocalDate convertedDate;

        if (dateParam == null || dateParam.isEmpty()) {

            convertedDate = LocalDate.now().minusDays(30);

        } else {

            convertedDate = LocalDate.parse(dateParam, formatter);
        }

        List<Page> spacePages = spaceManager.getSpace(conversionContext.getSpaceKey()).getHomePage().getDescendants();

        List<Page> actualPages = new ArrayList<>();

        for (Page page : spacePages) {

            Date lastModifDate = page.getLastModificationDate();

            LocalDate lastModifDateToLocal = lastModifDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            if (lastModifDateToLocal.isAfter(convertedDate)) {

                actualPages.add(page);
            }
        }

        context.put("ActualPages", actualPages);

        return VelocityUtils.getRenderedTemplate(TEMPLATE, context);

    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
